## Description

This git repository contains a set of SNPs identified in cork oak.

A total of 8,411 SNPs are included in the Qsuber_8411_SNPs.tsv. The SNP information is in the folowing format:

- 1rst column: SNP id
- 2nd column: 100bpOfLeftFlanquedSequence**[Reference_allele/Alternative_allele]**100bpOfRigthFlangedSequence

**NOTE:** The file Qsuber_8411_SNPs.tsv can be downloaded from the Download section of this repository.

